# Ce tutoriel est un exemple d'utilisation du package OSRM pour trouver 
# les distances de commune � commune pour la Corse
# Auteurs et autrices: alice.bergonzoni@sante.gouv.fr et julien.naour@sante.gouv.fr 

# Installation des packages

install.packages("maptools")
install.packages("sf")
install.packages("osrm")

# Chargement des packages

library("osrm")       # Permet d'utiliser l'API OSRM
library("sf")         # N�cessaire pour la lecture de fichier .shp
library("maptools")   # Utile pour la projection de lambert 93 vers wsg84


# Pr�paration des donn�es pour OSRM
# On part des donn�es de chef-lieu IGN => http://professionnels.ign.fr/adminexpress
# Ces donn�es sont projet�es en Lambert93, on va les ramener en latitude et longitude standard

villes=sf::st_read("I:/ECHANGE/NAOUR, Julien/Distancier/CHEF_LIEU.shp")

coords_lamb93=as.data.frame(matrix(unlist(villes$geometry), ncol = 2, byrow = TRUE))

names(coords_lamb93)=c("x", "y")
coordinates(coords_lamb93)=c("x","y")

# C'est ici que sont d�finies les deux projections, l'initiale pour lambert 93 et la nouvelle pour wsg84
wsg84=CRS("+init=epsg:4326")
lamb93=CRS("+init=epsg:2154")

# On d�fnit ainsi que les donn�es d'origine sont projet�es en Lambert 93
proj4string(coords_lamb93) = lamb93

# On passe les donn�es en wsg84
coords_wsg84=spTransform(coords_lamb93,wsg84)

# Etape qui facilite la relecture des donn�es g�ographiques
write.table(coords_wsg84,'I:/ECHANGE/NAOUR, Julien/Distancier/villes.csv')
coords_villes = read.table('I:/ECHANGE/NAOUR, Julien/Distancier/villes.csv')

# On cr�e un dataframe avec les codes communes associ�s aux coordonn�es projet�es en wsg84
coords=data.frame(code_com=as.character(villes$INSEE_COM), x=c_villes$x, y=c_villes$y, 
                 stringsAsFactors = FALSE)

# S�lection des communes corses qui commencent donc par 2A et 2B
Corse=subset(coord, (substr(code_com, 1, 2) %in% c("2A", "2B")))

# On param�tre le package pour utiliser le serveur DREES, ici le distancier voiture
options(osrm.server="http://10.200.15.24:5000/")

# On utilise l'API du package osrm
Dist_Corse=osrmTable(src=Corse, dst=Corse)

Dist_Corse$durations

# Exemple pour �crire la table de r�sultat: 
write.table(Dist_Corse$durations, "I:/ECHANGE/NAOUR, Julien/Distancier/corse_durations.csv")
